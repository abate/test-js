
## Install

    yarn

## Run Rest api (nodeJS)

    node api/app.js

Then go to http://localhost:3000/describe/block_header

## Run Vue.js client application

    yarn serve
