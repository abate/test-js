/* eslint-env node */

const codec = require('@nomadic-labs/tezos-codec')
var api = codec.init()

var express = require("express");
var app = express();
app.listen(3000, () => {
  console.log("Server running on port 3000");
});

app.get("/list", (req, res) => {
  api.then(function(api){
    res.json(api.listEncodings())
  })
});

app.get("/describe/:id", (req, res) => {
  api.then(function(api){
    res.end(JSON.stringify(api.describe(req.params.id)))
  })
});
